package com.animushome.heart.happs.examplehapp.servlet;

import org.osgi.service.component.annotations.*;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

import com.animushome.heart.happs.examplehapp.ExampleHApp;
import com.animushome.heart.service.HApp;


/**
 * This is the main class for providing a custom GUI for your hApp
 * 
 * This servlet will match everything that is requested on
 * http://<heart_ip>/happs/com.animushome.heart.happs.examplehapp/*
 * to static files (html, css, javascript) in the 'resources' folder.
 *
 */
@Component ( service = ExampleServlet.class,
property = {
HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PATTERN + "="+ HApp.HAPP_URI + "/" + ExampleHApp.HAPP_UID +"/*",
HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PREFIX + "=/resources"   
})
public class ExampleServlet {
	//This will only load our resources
}
