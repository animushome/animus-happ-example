Example Animus hApp
============================================

The purpose of this hApp is to only show you how the main parts of a hApp are interconnected, it doesn't have any higher purpose or function. 

There are three main parts involved in this hApp:

1. Get access to all light functions so the hApp can control these functions using their API. See the main hApp file `ExampleHApp.java`

2. Register a MultiLevelControl function so that other hApps can trigger setData actions on this hApp which will turn on the lights if the level reaches above 50% (the MultiLevelControl will not show up in the current GUI but its API is available). See the main file `ExampleHApp.java` and `MyLevelController.java`

3. Provide a custom GUI where a user can fill in a form to then toggle the lights (which we got access to from #1) based on the value entered by the user. See `rest`-package, `servlet`-package and `ExampleHApp.java` to accomplish this.
Once the hApp is installed you can view the custom GUI on `http://<heart_ip>/happs/com.animushome.heart.happs.examplehapp/index.html`


This includes the SDK to develop and deploy your builds easily to your Heart. Follow the steps below to get started.

You should use a terminal when running most of these commands. For Windows we recommend https://git-for-windows.github.io/

1. Clone the project with
`git clone ...`

2. Edit your Heart properties. Edit file:
`heart.properties`

3. Start Eclipse and import the cloned Bnd OSGi Workspace
`File->import->general->Existing project into Workspace`

4. Check that the project builds (it will fail because the test case will fail)
`gradle build`

5. Deploy to your Heart
`gradle deploy`


Remove from your Heart
`gradle remove`


For more information read our docs.
https://animushome.atlassian.net/wiki/display/AHK/Developer